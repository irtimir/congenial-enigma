from functools import wraps
from inspect import signature


def strict_argument_types(func):
    """
    Проверяет тип переданных в функцию аргументов
    Пример:
    def foo(a: str, b: int):
        ...

    Если переменная a - не str и b - не int, будет исключение TypeError
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        sig = signature(func)
        bind_args = sig.bind(*args, **kwargs)

        for key, attr in bind_args.arguments.items():
            annot = sig.parameters[key].annotation

            if annot != sig.empty and not isinstance(attr, annot):
                raise TypeError('The argument "{}" must be "{}", passed "{}"'.format(
                    key, annot, type(attr)))
        return func(*args, **kwargs)
    return wrapper


def strict_return_type(func):
    """
    Проверяет тип данных, который возвратила функция
    Пример:
    def foo(a: 'x', b: 5 + 6, c: list) -> str
        ...

    Если foo вернёт не str, будет исключение TypeError
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        annot = signature(func).return_annotation
        result = func(*args, **kwargs)

        if annot != signature(func).empty and not isinstance(result, annot):
            raise TypeError('The return value must be "{}", not "{}"'.format(
                annot, type(result)))
        return result
    return wrapper
